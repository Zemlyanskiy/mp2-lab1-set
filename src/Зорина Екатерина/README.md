### Цели и задачи

_Цель данной работы_ заключается в разработке структуры данных для хранения множеств с
использованием битовых полей, а также освоение таких инструментов разработки
программного обеспечения, как система контроля версий __Git__ и фрэймворк для
разработки автоматических тестов __Google Test__.

Перед выполнением работы мной был получен _проект-шаблон_, содержащий следующее:

 - Интерфейсы классов битового поля и множества (h-файлы)
 - Готовый набор тестов для каждого из указанных классов
 - Пример использования класса битового поля и множества для решения задачи
   поиска простых чисел с помощью алгоритма __"Решето Эратосфена"__

_Выполнение работы предполагает решение следующих задач:_

  1. Реализация класса битового поля __`TBitField`__ согласно заданному интерфейсу.
  1. Реализация класса множества __`TSet`__ согласно заданному интерфейсу.
  1. Обеспечение работоспособности тестов и примера использования.
  1. Реализация нескольких простых тестов на базе Google Test.
  1. Публикация исходных кодов в личном репозитории на BitBucket.

### Разработка класса битового поля __`TBitField`__

В данном классе объект битовое поле представляет собой структуру, состоящую из целочисленных переменных `BitLen` и `MemLen`, а также массива `*pMem` типа _TELEM_, который равносилен встроенному _unsigned int_. Переменная `BitLen` равна длине битового поля и задается пользователем. Исходя из нее вычисляется `MemLen` -- количество элементов `pMem` для хранения битового поля. В `pMem` будет храниться само битовое поле, представленное в виде массива из целых чисел. Работать непосредственно с битами мы будем через битовые маски. 

В классе реализованы методы доступа и работы с битами, различные операции над битовыми полями, а также операторы ввода-вывода.

Стоит отметить, что нумерация бит в битовой строке и элементов в массиве идет слева направо, а нумерация бит в каждом _TELEM_ -- справа налево.

Также при выполнении программы могут быть вызваны исключения: 
 - __"Incorrect length of bitfield"__ - некорректная длина битового поля
 - __"Incorrect index"__ - некорректный индекс бита
 - __"Bitfield is not exists"__ - не удалось создать битовое поле

#### Реализация класса __`TBitField`__
```
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#include "tbitfield.h"


TBitField::TBitField(int len)
{
	if (len <= 0)
		throw "Incorrect length of bitfield";
	BitLen = len;
	MemLen = (BitLen + 31) >> 5;
	//31 = длина TELEM в битах - 1
	// >> 5 равноисльно делению на 32, т. е. 2^5 
	//добавляем 31 для нелишения битов из конца поля 
	pMem = new TELEM [MemLen];
	if (pMem == NULL)
		throw "Bitfield is not exists";
	for (int i = 0; i < MemLen; i++)
		pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM [MemLen];
	for (int i = 0; i < MemLen; i++)
		pMem[i] = bf.pMem[i];
}

TBitField::~TBitField() // деструктор
{
	delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if (n < 0 || n >= BitLen)
		throw "Incorrect index";
	return n >> 5;
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	if (n < 0 || n >= BitLen)
		throw "Incorrect index";
	return (1 << (n % 31)); 
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
  return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	if (n < 0 || n >= BitLen)
		throw "Incorrect index";
	TELEM temp = GetMemMask(n);
	pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] | temp;
}

void TBitField::ClrBit(const int n) // очистить бит
{
	if (n < 0 || n >= BitLen)
		throw "Incorrect index";
	TELEM temp = GetMemMask(n);
	temp = ~ temp;
	pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] & temp;
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	if (n < 0 || n >= BitLen)
		throw "Incorrect index";
  return pMem[GetMemIndex(n)] & GetMemMask(n);
}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{	
	if (bf == *this)
		return *this;
	//защита от самоприсваивания
	if (pMem == NULL)
		throw "Bitfield is not exists";
	delete[] pMem;
	BitLen = bf.GetLength();
	MemLen = (BitLen + 31) >> 5;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
		pMem[i] = bf.pMem[i];
	return *this;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	if (GetLength() != bf.GetLength())
		return 0;
	for (int i = 0; i < MemLen; i++)
		if (pMem[i] != bf.pMem[i])
			return 0;
	return 1;
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
	if (GetLength() != bf.GetLength())
		return 1; 
	for (int i = 0; i < MemLen; i++)
		if (pMem[i] != bf.pMem[i])
			return 1;
	return 0;
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	TBitField Temp(max(BitLen, bf.BitLen));
	for (int i = 0; i < max(MemLen, bf.MemLen); i++)
		Temp.pMem[i] = pMem[i] | bf.pMem[i];
	return Temp;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	TBitField Temp(max(BitLen, bf.BitLen));
	for (int i = 0; i < min(BitLen, bf.BitLen); i++)
		Temp.pMem[i] = pMem[i] & bf.pMem[i];
	return Temp;
}

TBitField TBitField::operator~(void) // отрицание
{
	TBitField Temp(BitLen);
	for (int i = 0; i < BitLen; i++)
	{
		if (GetBit(i) == 0) 
			Temp.SetBit(i);
		else
			Temp.ClrBit(i);
	}
	return Temp;
}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
	char b;
	int i = 0;
	while (true)
	{
		istr >> b;
		if (b == '0')
			bf.ClrBit(i);
		else if (b == '1')
			bf.SetBit(i);
		else 
			break;
		i++;
	}
	bf.BitLen = i;
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	for (int i = 0; i < bf.BitLen; i++)
		if (bf.GetBit(i))
			ostr << '1';
		else
			ostr << '0';
	return ostr;
}
```

### Разработка класса множества __`TSet`__

Класс множства __`TSet`__ является примером использования битовых полей. В его данные входит целочисленная переменная `MaxPower`, то есть максимальная мощность множества, а также `BitField` - экземпляр класса __`TBitField`__. То есть, реализация __`TSet`__ происходит через наследование __`TBitField`__.

В классе реализованы методы доступа и работы с элементами, различные теоретико-множественные операции, конструкторы преобразования в битовое поле и наоборот, а также операторы ввода-вывода.

Также при выполнении программы могут быть вызваны исключения:
 - __"Incorrect element"__ - некорректный элемент множества

 #### Реализация класса __`TSet`__
``` 
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tset.cpp - Copyright (c) Гергель В.П. 04.10.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Множество - реализация через битовые поля
#include <iostream>
using namespace std;
#include "tset.h"

TSet::TSet(int mp) : BitField(mp)
{
	MaxPower = mp;
}

// конструктор копирования
TSet::TSet(const TSet &s) : BitField(s.BitField)
{
	MaxPower = s.MaxPower;
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : BitField(bf)
{
	MaxPower = bf.GetLength();
}

TSet::operator TBitField()
{
	return TBitField(MaxPower);
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
	if (BitField.GetBit(Elem))
		return 1;
	return 0;
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	if (Elem < 0 || Elem > MaxPower)
		throw "Incorrect element";
	BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	if (Elem < 0 || Elem > MaxPower)
		throw "Incorrect element";
	BitField.ClrBit(Elem);
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание
{
	MaxPower = s.MaxPower;
	BitField = s.BitField;
	return *this;
}

int TSet::operator==(const TSet &s) const // сравнение
{
	return ((MaxPower == s.MaxPower) & (BitField == s. BitField));
}

int TSet::operator!=(const TSet &s) const // сравнение
{
	return !((MaxPower == s.MaxPower) & (BitField == s. BitField));
}

TSet TSet::operator+(const TSet &s) // объединение
{
	TSet Temp = (BitField | s.BitField);
	return Temp;
}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	if (Elem < 0 || Elem > MaxPower)
		throw "Incorrect element";
	TSet result = *this;
	result.BitField.SetBit(Elem);
	return result;
}

TSet TSet::operator-(const int Elem) // разность с элементом
{
	if (Elem < 0 || Elem > MaxPower)
		throw "Incorrect element";
	BitField.ClrBit(Elem);
	return *this;
}

TSet TSet::operator*(const TSet &s) // пересечение
{
	TSet Temp(max(s.MaxPower, MaxPower));
	Temp.BitField = BitField & s.BitField;
	return Temp;
}

TSet TSet::operator~(void) // дополнение
{
	return (~BitField);
}

// перегрузка ввода/вывода

istream &operator>>(istream &istr, TSet &s) // ввод
{
	//формат ввода: { A1 A2 A3 ... An }
	int temp = 0;
	char b;
	while (b != '}')
	{
		istr >> temp;
		s.InsElem(temp);
	}
	return istr;
}

ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{
	//формат вывода: { A1 A2 A3 ... An }
	ostr << "{ ";
	for(int i = 0; i < s.MaxPower; i++)
		if (s.IsMember(i))
			ostr << i << ' ';
	ostr << '}';
	return ostr;
}
```
 
 ### Тестирование
 
 Ранее готовый набор тестов на базе __Google Test__ для классов __`TBitField`__ и __`TSet`__ успешно пройден:
 [TBitField](TBitField.png)
 
 [TSet](TSet.png)
 
 Также была опробована программа __"Решето Эратосфена"__ на битовом поле:
 [Resheto](Resheto.png)
 
 ### Выводы
 
 В ходе выполнения работы мы реализовали класс битового поля, используя такие возможности объектно-ориентированного подхода как перегрузка операций и дружественные функции. При создании класса множества также было использовано наследование классов.
 
 Мы поработали с системой тестирования на базе __Google Test__, которая заметно упрощает поиск ошибок в тексте программы, но, к сожалению, не может гарантировать правильность ее работы. Также проверить работоспособность помогла программа  __"Решето Эратосфена"__.