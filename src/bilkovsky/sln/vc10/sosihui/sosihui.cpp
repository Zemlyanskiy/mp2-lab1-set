#include "stdafx.h"
#include <iostream>

using namespace std;

typedef unsigned int TELEM;

class TBitField
{
private:
  int  BitLen; // ����� �������� ���� - ����. �-�� �����
  TELEM *pMem; // ������ ��� ������������� �������� ����
  int  MemLen; // �-�� ��-��� ��� ��� ������������� ���.����

  // ������ ����������
  int   GetMemIndex(const int n) const; // ������ � p��� ��� ���� n       (#�2)
  TELEM GetMemMask (const int n) const; // ������� ����� ��� ���� n       (#�3)
public: 
  TBitField(int len);                //                                   (#�1)
  TBitField(const TBitField &bf);    //                                   (#�1)
  ~TBitField();                      //                                    (#�)

  // ������ � �����
  int GetLength(void) const;      // �������� ����� (�-�� �����)           (#�)
  void SetBit(const int n);       // ���������� ���                       (#�4)
  void ClrBit(const int n);       // �������� ���                         (#�2)
  int  GetBit(const int n) const; // �������� �������� ����               (#�1)

  // ������� ��������
  int operator==(const TBitField &bf) const; // ���������                 (#�5)
  int operator!=(const TBitField &bf) const; // ���������
  TBitField& operator=(const TBitField &bf); // ������������              (#�3)
  TBitField  operator|(const TBitField &bf); // �������� "���"            (#�6)
  TBitField  operator&(const TBitField &bf); // �������� "�"              (#�2)
  TBitField  operator~(void);                // ���������                  (#�)

  friend istream &operator>>(istream &istr, TBitField &bf);       //      (#�7)
  friend ostream &operator<<(ostream &ostr, const TBitField &bf); //      (#�4)
};
// ��������� �������� �������� ����
//   ���.���� - ����� ����� � �������� �� 0 �� BitLen
//   ������ p��� ��������������� ��� ������������������ MemLen ���������
//   ���� � ��-��� p��� ���������� ������ ������ (�� ������� � �������)
// �8 �2 �4 �2

//#include "tbitfield.h"
inline int Counter()
{
	int count = 0, size = sizeof(TELEM) * 8;
	while (size >>= 1)
		count++;
	return count;
}

TBitField::TBitField(int len)
{
	if (len <= 0)
		throw "�������� ������ ����";
	BitLen = len;
	MemLen = (BitLen + sizeof(TELEM) * 8 - 1) >> Counter();
	pMem = new TELEM[MemLen];
	if (pMem != NULL)
		for (int i = 0; i < MemLen; i++)
			pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // ����������� �����������
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	if (bf.pMem != NULL)
		for (int i = 0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];
}

TBitField::~TBitField()
{
	delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // ������ ��� ��� ���� n
{
	if (n > BitLen || n <= 0)
		throw "����� �� ������� ����";
	return n >> Counter();
}

TELEM TBitField::GetMemMask(const int n) const // ������� ����� ��� ���� n
{
	if (n > BitLen || n <= 0)
		throw "����� �� ������� ����";
	return 1 << (n % (sizeof(TELEM) * 8));
}

// ������ � ����� �������� ����

int TBitField::GetLength(void) const // �������� ����� (�-�� �����)
{
  return BitLen;
}

void TBitField::SetBit(const int n) // ���������� ���
{
	if (n > BitLen || n <= 0)
		throw "����� �� ������� ����";
	pMem[GetMemIndex(n)] |= GetMemMask(n);
}

void TBitField::ClrBit(const int n) // �������� ���
{
	if (n > BitLen || n <= 0)
		throw "����� �� ������� ����";
	pMem[GetMemIndex(n)] &= (~GetMemMask(n));
}

int TBitField::GetBit(const int n) const // �������� �������� ����
{
	if (n >= BitLen || n < 0)
		throw "����� �� ������� ����";
	return pMem[GetMemIndex(n)] & GetMemMask(n) ? 1:0;
}

// ������� ��������

TBitField& TBitField::operator=(const TBitField &bf) // ������������
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	delete[] pMem;
	pMem = new TELEM[MemLen];
	if (bf.pMem != NULL)
		for (int i = 0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];
	return *this;
}

int TBitField::operator==(const TBitField &bf) const // ���������
{
	if(BitLen==bf.BitLen)
	{
		for (int i = 0; i < MemLen; i++)
			if (pMem[i] != pMem[i])
				return 0;
			return 1;
	}
	return 0;
}

int TBitField::operator!=(const TBitField &bf) const // ���������
{
	if (BitLen == bf.BitLen)
	{
		for (int i = 0; i < MemLen; i++)
			if (pMem[i] != pMem[i])
				return 1;
		return 0;
	}
	return 0;
  return 1;
}

TBitField TBitField::operator|(const TBitField &bf) // �������� "���"
{
	int len = BitLen;
	if (bf.BitLen > BitLen)
		len = bf.BitLen;
	TBitField temp(len);
	for (int i = 0; i < MemLen; i++)
		temp.pMem[i] = pMem[i];
	for (int i = 0; i < bf.MemLen; i++)
		temp.pMem[i] |= bf.pMem[i];
	return temp;
}

TBitField TBitField::operator&(const TBitField &bf) // �������� "�"
{
	int len = BitLen;
	if (bf.BitLen > BitLen)
		len = bf.BitLen;
	TBitField temp(len);
	for (int i = 0; i < MemLen; i++)
		temp.pMem[i] = pMem[i];
	for (int i = 0; i < bf.MemLen; i++)
		temp.pMem[i] &= bf.pMem[i];
	return temp;
}

TBitField TBitField::operator~(void) // ���������
{
	for (int i = 0; i < MemLen; i++)
		pMem[i] = ~pMem[i];
	return *this;
}

// ����/�����

istream &operator>>(istream &istr, TBitField &bf) // ����
{
	int i = 0,l = 1; char bit;
	while (l & (i<=bf.BitLen))
	{
		istr >> bit;
		switch (bit)
		{
			case'0':
				bf.ClrBit(i++);
				break;
			case'1':
				bf.GetBit(i++);
				break;
			case' ':
				break;
			default:
				l = 0;
				break;
		}
	}
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // �����
{
	for (int i = 0; i < bf.BitLen; i++)
		if (bf.GetBit(i))
			ostr << '1';
		else
			ostr << '0';
	return ostr;
}



