// ����, ���, ���� "������ ����������������-2", �++, ���
//
// tbitfield.cpp - Copyright (c) ������� �.�. 07.05.2001
//   ������������ ��� Microsoft Visual Studio 2008 �������� �.�. (19.04.2015)
//
// ������� ����
// ������ 1. ������������ ��������
// ������ 2. ����� �� ������� ��������������
// ������ 3. �������� �������
// ������ 4. ������������ ������
// 32= (sizeof(TELEM)*8)
// Use of (sizeof(TELEM)*8) for making program more universal. It will be enough to change TELEM. For example, write char or short

#include "tbitfield.h"

TBitField::TBitField(int len):BitLen(len)// bitfield begining the account from 0 to len-1
{
	if (len > 0)
	{
		MemLen = (len + (sizeof(TELEM) * 8) * (len % (sizeof(TELEM) * 8) != 0) - len % (sizeof(TELEM) * 8)) / (sizeof(TELEM) * 8);
		pMem = new TELEM[MemLen];
		if (pMem != nullptr)
			for (int i = 0;i < MemLen;i++)
				pMem[i] = 0;//make all bits equal zero
		else
			throw 4;//if there is no memory throw error message
	}
	else
		throw 1;//if there is len <= 0 throw error message
}

TBitField::TBitField(const TBitField &bf) // ����������� �����������
{
	MemLen = bf.MemLen;
	BitLen = bf.BitLen;
	pMem = new TELEM[MemLen];
	if (pMem != nullptr)
		for (int i = 0;i < MemLen;i++)
			pMem[i] = bf.pMem[i];
	else
		throw 4;//if there is no memory throw error message
}

TBitField::~TBitField()
{
	delete[]pMem;
}

int TBitField::GetMemIndex(const int n) const // ������ ��� ��� ���� n
{
	if ((n >= 0) && (n < BitLen))//check of borders
		return (n + (sizeof(TELEM) * 8) * (n % (sizeof(TELEM) * 8) != 0) - n % (sizeof(TELEM) * 8)) / (sizeof(TELEM) * 8) - 1 * (n != 0)*(n%(sizeof(TELEM)*8)!=0);
	else
		throw 2;//if there is a error of indexing throw error message
}

TELEM TBitField::GetMemMask(const int n) const // ������� ����� ��� ���� n
{
	if ((n >= 0) && (n < BitLen))
		return (TELEM)(1 << (n % (sizeof(TELEM) * 8)));
	else
		throw 2;//if there is a error of indexing throw error message
}

// ������ � ����� �������� ����

int TBitField::GetLength(void) const // �������� ����� (�-�� �����)
{
	return BitLen;
}

void TBitField::SetBit(const int n) // ���������� ���
{
	if ((n >= 0) && (n < BitLen))
		pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] | GetMemMask(n);
	else
		throw 2;//if there is a error of indexing throw error message
}

void TBitField::ClrBit(const int n) // �������� ���
{
	if ((n >= 0) && (n < BitLen))
		pMem[GetMemIndex(n)] = pMem[GetMemIndex(n)] & (~GetMemMask(n));
	else
		throw 2;//if there is a error of indexing throw error message
}

int TBitField::GetBit(const int n) const // �������� �������� ����
{
	if ((n >= 0) && (n < BitLen))
		return ((pMem[GetMemIndex(n)] & GetMemMask(n)) != 0);
	else
		throw 2;//if there is a error of indexing throw error message
}

// ������� ��������

TBitField& TBitField::operator=(const TBitField &bf) // ������������
{
	if (&bf != this)
	{
		delete[]pMem;
		MemLen = bf.MemLen;
		BitLen = bf.BitLen;
		pMem = new TELEM[MemLen];
		if (pMem != nullptr)
		{
			for (int i = 0;i < MemLen;i++)
				pMem[i] = bf.pMem[i];
			return (*this);
		}
		else
			throw 4;//if there is no memory throw error message
	}
	else
		throw 3;//if there is a conflict of addresses throw error message
}

int TBitField::operator==(const TBitField &bf) const // ���������
{
	int i = 0;//i==result
	if (BitLen == bf.BitLen)
	{
		while ((i < MemLen) && (bf.pMem[i] == pMem[i]))
			i++;
		if (i != MemLen)
			i = 0;
		else
			i = 1;
	}
	return i;
}

int TBitField::operator!=(const TBitField &bf) const // ���������
{
	return !(*this == bf);
}

TBitField TBitField::operator|(const TBitField &bf) // �������� "���"
{
	TBitField temp((BitLen>bf.BitLen)?BitLen:bf.BitLen);//the largest BitLen
	for (int i = 0;i < temp.MemLen;i++)
		temp.pMem[i] = pMem[i] | bf.pMem[i];
	return temp;
}

TBitField TBitField::operator&(const TBitField &bf) // �������� "�"
{
	TBitField temp((BitLen>bf.BitLen) ? BitLen : bf.BitLen);//the largest BitLen
	for (int i = 0;i < temp.MemLen;i++)
		temp.pMem[i] = pMem[i] & bf.pMem[i];
	return temp;
}

TBitField TBitField::operator~(void) // ���������
{
	TBitField temp(*this);
	if (MemLen != 1)
		for (int i = 0;i < MemLen - 1;i++)
			temp.pMem[i] = ~temp.pMem[i];
	for (int i = (MemLen-1)*sizeof(TELEM)*8;i < BitLen;i++)
		if (temp.GetBit(i) == 0)
			temp.SetBit(i);
		else
			temp.ClrBit(i);
	return temp;
}

// ����/�����

istream &operator>>(istream &istr, TBitField &bf) // ����
{
	int i =  0;
	char ch;
	while (i <bf.BitLen)
	{
		istr >> ch;
		if (ch == '0')
			bf.ClrBit(i++);
		else if (ch == '1')
			bf.SetBit(i++);
		else
			break;
	}
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // �����
{
	int i = 0;
	while (i <bf.BitLen)
		ostr << bf.GetBit(i++);
	return ostr;
}
