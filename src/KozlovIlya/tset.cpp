// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tset.cpp - Copyright (c) Гергель В.П. 04.10.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Множество - реализация через битовые поля

#include "tset.h"

TSet::TSet(int mp) : BitField(mp)
{
	MaxPower = mp;
}

// конструктор копирования
TSet::TSet(const TSet &s) : BitField(s.BitField)
{
	MaxPower = s.MaxPower;
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : BitField(bf)
{
	MaxPower = bf.GetLength();
}

TSet::operator TBitField()
{
	return BitField;
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
		if (Elem < 0)
	{
		throw "\n\nError 3! This element does not exist!\n\n";
	}


	return BitField.GetBit(Elem);
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	if (Elem < 0)
	{
		throw "\n\nError 3! This element does not exist!\n\n";
	}

	BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	if (Elem < 0)
	{
		throw "\n\nError 3! This element does not exist!\n\n";
	}

	BitField.ClrBit(Elem);
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание
{
	MaxPower = s.MaxPower;
	BitField = s.BitField;
	return *this;
}

int TSet::operator==(const TSet &s) const // сравнение
{
	if(BitField == s.BitField)
		return 1;
	return 0;
}

int TSet::operator!=(const TSet &s) const // сравнение
{
	if(BitField == s.BitField)
		return 0;
	return 1;
}

TSet TSet::operator+(const TSet &s) // объединение
{
	TSet DATA(BitField | s.BitField);
		return DATA;

}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	TSet DATA = *this;
	DATA.InsElem(Elem);
	return DATA;
}

TSet TSet::operator-(const int Elem) // разность с элементом
{
	TSet DATA = *this;
	DATA.DelElem(Elem);
	return DATA;

}

TSet TSet::operator*(const TSet &s) // пересечение
{
	TSet DATA(BitField & s.BitField);
	return DATA;
}

TSet TSet::operator~(void) // дополнение
{
	TSet DATA = ~BitField;
	return DATA;
}

// перегрузка ввода/вывода

istream &operator>>(istream &istr, TSet &s) // ввод
{
	int el;
	char data;

	cout<<"\n\n{";

	for (int i=0; i<s.MaxPower; i++)
	{
		tryagain:
		istr>>data;

		if (isdigit(data))
		{
			el=data;
			s.InsElem(el);
			cout<<" , ";
		}

		else
			cout<<'\t';
			goto tryagain;
	}

	cout<<"}\n\n";
	return istr;
}

ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{
	bool first = true;
	ostr <<"{";
	for(int i = 0; i < s.MaxPower; i++)
		{
			if(s.IsMember(i))
			{
				if(!first)
					ostr <<" , ";
				else
					first = false;
				ostr << i;
			}
		}
	ostr <<"}";
	return ostr;
}
